package de.bstachmann.rpgdiceutils


import org.apache.commons.math3.fraction.Fraction
import org.apache.commons.math3.fraction.Fraction.*
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlin.random.Random


class Roll<T : Outcome>(val outcomes: List<T>, val name: String = "Unknown") {


    init {
        require(outcomes.isNotEmpty()) { "There should be $outcomes in a roll." }
        val sumP = outcomes.fold(ZERO) { sum, outcome -> sum.add(outcome.p) }
        require(sumP == ONE) { "Sum of all outcomes ${sumP} is not 1!" }
    }


    override fun toString(): String {
        return "$name [${outcomes.map(Outcome::shortDescription).joinToString(",")}]"
    }


    fun description(): String =
        "## $name\n${outcomes.map { it.details() }.joinToString("\n")}\n"


    fun perform(random: Random = Random): Outcome {
        val r = Fraction(random.nextDouble())
        var p = ZERO
        return outcomes.find {
            p = p.add(it.p)
            p >= r
        } ?: throw IllegalStateException("Should always bei able to generate outcome for ${description()}. p=$p r=$r")
    }


    fun <U : Outcome> mapTo(t: Roll<U>): Roll<MappedOutcome<T, U>> {
        val remainingTargets = t.outcomes.toMutableList()
        var sourceCoverage = ZERO
        var targetCoverage = ZERO

        val mappedOutcomes = this.outcomes.map { source ->

            sourceCoverage = sourceCoverage.add(source.p)

            val targets = mutableListOf<U>()

            while (true) {
                val nextTarget = remainingTargets.firstOrNull() ?: break
                val sourceDeficit = sourceCoverage.subtract(targetCoverage)
                val targetOverflow = nextTarget.p.subtract(sourceDeficit)

                val middleOfNextTarget = targetCoverage.add(nextTarget.p.divide(2))

                if (targetOverflow < sourceDeficit
                    || source == outcomes.last()
                    // For symmetry: Always round towards middle of interval
                    || (targetOverflow == sourceDeficit && (middleOfNextTarget <= ONE_HALF))
                ) {
                    targetCoverage = targetCoverage.add(nextTarget.p)
                    remainingTargets.removeAt(0)
                    targets.add(nextTarget)
                } else {
                    break
                }
            }

            MappedOutcome(source, targets)

        }

        assert(sourceCoverage == ONE)
        assert(targetCoverage == ONE)

        assert(remainingTargets.isEmpty()) { "All targets should be mapped, but $remainingTargets is not empty!" }

        val stddev = sqrt(mappedOutcomes.map { it.deviation() * it.deviation() }.average())
        val isOk = if (stddev < 0.15) "OK" else ""

        return Roll<MappedOutcome<T, U>>(mappedOutcomes, name)
    }


    fun makeDramatic(): Roll<DramaRollOutcome<T>> {
        val lowest = outcomes.first()
        val highest = outcomes.last()
        return Roll(outcomes.flatMap { outcome ->
            when (outcome) {
                lowest, highest ->
                    outcomes.map { secondOutcome ->
                        secondOutcome.p
                        DramaRollOutcome(
                            outcome,
                            secondOutcome
                        )
                    }
                else ->
                    listOf(DramaRollOutcome(outcome, null))
            }
        })
    }

    operator fun plus(other: Roll<T>): Roll<PlainNumericOutcome> =
        Roll(
            outcomes
                .flatMap { first ->
                    other.outcomes.map { second ->
                        PlainNumericOutcome(
                            first.value.add(second.value),
                            first.p.multiply(second.p)
                        )
                    }
                }
                .groupBy { it.value }
                .map { (k, values) ->
                    PlainNumericOutcome(
                        values.fold(ZERO) { acc, o -> acc.add(o.value) },
                        values.fold(ZERO) { acc, o -> acc.add(o.p) }
                    )
                },
            "$name + ${other.name}"
        )


    companion object {

        val STANDARD_DICE = listOf(4, 6, 8, 10, 12, 20).map { Roll.D(it) }

        fun D(n: Int) = Roll(
            (1..n).map { UniformFairDieOutcome(it, n) },
            "D$n"
        )

    }
}


infix fun Int.outOf(total: Int) = Fraction(this, total)


interface Outcome {

    val value: Fraction

    val p: Fraction

    val shortDescription: String

    fun details(): String

}



data class PlainNumericOutcome(
    override val value: Fraction,
    override val p: Fraction
) : Outcome {

    override val shortDescription: String get() = value.toString()

    override fun details(): String = "$value ${formatPercent(p)}"

}


data class SuccessFailureOutcome(
    override val value: Fraction,
    override val p: Fraction,
    override val shortDescription: String
) : Outcome {

    override fun details(): String =
        "$value $shortDescription: ${formatPercent(p)}"

}


data class UniformFairDieOutcome(
    val face: Int,
    val numFaces: Int
) : Outcome {

    override val p: Fraction get() = 1 outOf numFaces

    override val shortDescription: String get() = face.toString()

    override fun details(): String = "$face od D$numFaces (${formatPercent(p)})"

    override val value: Fraction get() = Fraction(face)

}


data class DramaRollOutcome<T : Outcome>(
    val firstOutcome: T,
    val secondOutcome: T?
) : Outcome {


    override val value: Fraction
        get() =
            if (secondOutcome != null)
                firstOutcome.value.add(firstOutcome.p.multiply(secondOutcome.value))
            else
                firstOutcome.value


    override val p: Fraction =
        firstOutcome.p.multiply(secondOutcome?.p ?: ONE)


    override val shortDescription: String
        get() =
            if (secondOutcome != null)
                "${firstOutcome.shortDescription}/${secondOutcome.shortDescription}"
            else
                firstOutcome.shortDescription


    override fun details(): String =
        "$shortDescription (${formatPercent(firstOutcome.p)})"


}


data class MappedOutcome<T : Outcome, U : Outcome>(
    val sourceOutcome: T,
    val targetsOutcomes: List<U>
) : Outcome {


    override val value: Fraction get() = sourceOutcome.value


    override val p: Fraction
        get() = targetsOutcomes.fold(ZERO) { acc, outcome -> acc.add(outcome.p) }


    override val shortDescription: String
        get() =
            "${sourceOutcome.shortDescription}:" +
                    " ${targetsOutcomes.firstOrNull()?.shortDescription} -" +
                    " ${targetsOutcomes.lastOrNull()?.shortDescription}"


    override fun details(): String =
        "${sourceOutcome.shortDescription}" +
                " ${targetsOutcomes.firstOrNull()?.shortDescription?.padStart(4)} -" +
                " ${targetsOutcomes.lastOrNull()?.shortDescription?.padEnd(4)}" +
                " p=${formatPercent(p)}" +
                " pw=${formatPercent(sourceOutcome.p)}" +
                " e=${(deviation() * 100.0).roundToInt().toString().padStart(2)}%"


    fun deviation() =
        abs((sourceOutcome.p.toDouble() - p.toDouble()) / sourceOutcome.p.toDouble())

}


fun successTable(probability: Fraction, label: String = "Success Table"): Roll<SuccessFailureOutcome> {
    val p = probability
    return Roll(
        listOf(
            SuccessFailureOutcome(Fraction(1), (ONE.subtract(p)).multiply(Fraction(1, 6)), "*** Failure"),
            SuccessFailureOutcome(Fraction(2), (ONE.subtract(p)).multiply(Fraction(2, 6)), " ** Failure"),
            SuccessFailureOutcome(Fraction(3), (ONE.subtract(p)).multiply(Fraction(3, 6)), "  * Failure"),
            SuccessFailureOutcome(Fraction(4), p.multiply(Fraction(3, 6)), "  * Success"),
            SuccessFailureOutcome(Fraction(5), p.multiply(Fraction(2, 6)), " ** Success"),
            SuccessFailureOutcome(Fraction(6), p.multiply(Fraction(1, 6)), "*** Success")
        ),
        label
    )
}


private fun formatPercent(d: Double) = "%03.1f%%".format(d * 100.0)

private fun formatPercent(f: Fraction) = formatPercent(f.toDouble())


