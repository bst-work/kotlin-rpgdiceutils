package de.bstachmann.rpgdiceutils

import io.kotlintest.specs.StringSpec


class CombiningDiceTest : StringSpec({
    "Combine dice".config(enabled = true) {

        println((Roll.D(6) + Roll.D(6)).description())
        println((Roll.D(2) + Roll.D(2)).description())
        println(Roll.D(6).makeDramatic())
        println((Roll.D(6).makeDramatic() + Roll.D(6).makeDramatic()).description())
    }
})