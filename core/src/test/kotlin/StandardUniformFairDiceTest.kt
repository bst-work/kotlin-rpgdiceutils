package de.bstachmann.rpgdiceutils

import de.bstachmann.tolerances.`±`
import de.bstachmann.tolerances.percent
import io.kotlintest.assertSoftly
import io.kotlintest.inspectors.forAll
import io.kotlintest.matchers.numerics.shouldBeInRange
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.apache.commons.math3.fraction.Fraction
import kotlin.math.roundToInt
import kotlin.random.Random

class StandardUniformFairDiceTest : StringSpec({

    val dice: List<Roll<UniformFairDieOutcome>> = Roll.STANDARD_DICE
    val numRuns = 20000

    val random = Random(42)


    "naming of dice" {
        dice.forAll { die -> die.name shouldBe "D${die.outcomes.size}" }
    }


    "face probabilites should add up to 100%" {
        dice.forAll { die -> die.outcomes.fold(Fraction.ZERO) { acc, o -> acc.add(o.p) } shouldBe Fraction.ONE }
    }


    "single face p" {
        assertSoftly {
            (1..100).forEach {
                val die = dice.random(random)
                val wantedOutcome = die.outcomes.random(random)
                val expectedHits = (numRuns * wantedOutcome.p.toDouble()).roundToInt()
                (1..numRuns).count { die.perform(random) == wantedOutcome } shouldBeInRange (expectedHits `±` 10.percent)
            }
        }
    }

    "dramatic dice probabilites should add up to 100%" {
        dice.forAll { die ->
            val makeDramatic = die.makeDramatic()
            makeDramatic.outcomes.fold(Fraction.ZERO) { acc, o -> acc.add(o.p) } shouldBe Fraction.ONE
        }
    }

})