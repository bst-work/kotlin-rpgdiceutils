package de.bstachmann.tolerances

import io.kotlintest.Matcher
import io.kotlintest.Result
import io.kotlintest.should
import io.kotlintest.shouldNot
import kotlin.math.roundToInt


// Percentages

data class Percentage(val percents: Double) {

    override fun toString(): String {
        return "%03.1f%%".format(percents)
    }
}

val Double.percent: Percentage get() = Percentage(this)

fun Double.toPercentage(): Percentage = Percentage(this * 100.0)

val Int.percent: Percentage get() = Percentage(this.toDouble())

// Tolerances - Int


@Suppress("FunctionName", "NonAsciiCharacters")
infix fun Int.`±`(absoluteTolerance: Int): IntRange = tolerance(absoluteTolerance)

infix fun Int.tolerance(absoluteTolerance: Int): IntRange {
    require(absoluteTolerance >= 0)
    return (this - absoluteTolerance)..(this + absoluteTolerance)
}

infix fun Int.tolerance(relative: Percentage): IntRange = tolerance((this * relative.percents / 100.0).roundToInt())

@Suppress("FunctionName", "NonAsciiCharacters")
infix fun Int.`±`(relativeTolerance: Percentage): IntRange = this.tolerance(relativeTolerance)

// Tolerances - Double

@Suppress("FunctionName", "NonAsciiCharacters")
infix fun Double.`±`(absoluteTolerance: Double): ClosedFloatingPointRange<Double> = tolerance(absoluteTolerance)

infix fun Double.tolerance(absoluteTolerance: Double): ClosedFloatingPointRange<Double> {
    require(absoluteTolerance >= 0)
    return (this - absoluteTolerance)..(this + absoluteTolerance)
}

@Suppress("FunctionName", "NonAsciiCharacters")
infix fun Double.`±`(relativeTolerance: Percentage): ClosedFloatingPointRange<Double> = tolerance(relativeTolerance)

infix fun Double.tolerance(relative: Percentage): ClosedFloatingPointRange<Double> =
    tolerance(this * relative.percents / 100.0)

// Matcher

infix fun Double.shouldBeInRange(range: ClosedFloatingPointRange<Double>) = this should beInRange(range)
infix fun Double.shouldNotBeInRange(range: ClosedFloatingPointRange<Double>) = this shouldNot beInRange(range)
fun beInRange(range: ClosedFloatingPointRange<Double>) = object : Matcher<Double> {
    override fun test(value: Double): Result =
        Result(
            value in range,
            "$value should be in range $range",
            "$value should not be in range $range"
        )
}





