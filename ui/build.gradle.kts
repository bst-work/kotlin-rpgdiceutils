dependencies {

    implementation(project(":core"))

    val ktorVersion = "1.1.2"

    implementation("io.ktor:ktor-html-builder:$ktorVersion")
    implementation("io.ktor:ktor-http:$ktorVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
}
