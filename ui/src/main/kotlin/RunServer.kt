import de.bstachmann.rpgdiceutils.*
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.*
import org.apache.commons.math3.fraction.Fraction
import org.apache.commons.math3.fraction.Fraction.*
import kotlin.math.roundToInt

fun main() {
    embeddedServer(Netty, 8080) {
        routing {
            get("/") {
                call.respondHtml {
                    body {
                        // 63.5x88 mm
                        // 240 x 333
                        val d12 = Roll.D(12).makeDramatic()
                        listOf(
                            3 to "Eigentlich\nunmöglich",
                            12 to "Kaum zu schaffen",
                            24 to "Sehr schwierig",
                            48 to "Schwierig",
                            72 to "Fifty-Fifty",
                            144 - 48 to "Knifflig",
                            144 - 24 to "Wird schon klappen",
                            144 - 12 to "Fast sicher",
                            144 - 3 to "Routine"
                        )
                            .map { (n, label) -> Fraction(n, 144) to label }
                            .map { (p, label) -> successTable(p, label).mapTo(d12) }
                            .chunked(3)
                            .forEach {
                                it.forEach { drawTableCard(240,333, it) }
                                br()
                            }
                    }
                }
            }
        }
    }.start(true)
}

private fun BODY.drawTableCard(
    width: Int,
    height: Int,
    table: Roll<MappedOutcome<SuccessFailureOutcome, DramaRollOutcome<UniformFairDieOutcome>>>
) {
    svg {
        attributes["width"] = "$width"
        attributes["height"] = "$height"
        unsafe {
            val u = width / 12
            +"""<rect x="0" y="0" width="$width" height="$height" rx="${u/2}" ry="${u/2}" fill="black"/>"""
            val border = u
            val titleHeight = 5 * u / 2
            +"""<text x="${width/2}" y="${border + 3*u/4}" font-family="Arial" text-anchor="middle" font-size="$u" font-weight="bold" fill="white">${table.name}</text>"""

            val percent = table
                .outcomes.subList(3,6)
                .flatMap { it.targetsOutcomes }
                .fold(ZERO) { acc, o -> acc.add(o.p)}
                .multiply(100)
                .toDouble()
                .roundToInt()

            +"""<text x="${width/2}" y="${border + 2*u}" font-family="Arial" text-anchor="middle" font-size="${2*u/3}" fill="white">$percent%</text>"""

            +"""<g transform="translate($border,${border + titleHeight})">"""
            drawContent(width - border*2, height - border*2 - titleHeight, u , table)
            +"""</g>"""

        }
    }
}

private fun Unsafe.drawContent(
    width: Int,
    height: Int,
    u: Int,
    table: Roll<MappedOutcome<SuccessFailureOutcome, DramaRollOutcome<UniformFairDieOutcome>>>
) {

    val colors = listOf("#11DD00", "#22AA11", "#447722", "#774422", "#AA2211", "#DD1100")

    var p = ZERO
    var n = 1

    table.outcomes.asReversed().forEach { outcome ->
        val pnext = p.add(outcome.p)
        drawSection(
            p.multiply(height).toInt(),
            height * (n - 1) / 6,
            pnext.multiply(height).toInt(),
            height * n / 6,
            width,
            colors[n - 1 % colors.size]
        )
        p = pnext
        n += 1
    }

    n = 1
    table.outcomes.asReversed().forEach { outcome ->
        val pnext = p.add(outcome.p)
        val y = height * (n - 1) / 6
        val yNext = height * n / 6
        drawRequiredDice(outcome.targetsOutcomes.firstOrNull(), width, u * 2/3, 2 + (y + yNext) / 2)
        p = pnext
        n += 1
    }
}

private fun Unsafe.drawSection(
    y1l: Int,
    y1r: Int,
    y2l: Int,
    y2r: Int,
    width: Int,
    color: String
) {
    val marginL =  15 * width / 100
    val ml = 5 * width / 10
    val mr = 8 * width / 10
    val marginR = 85 * width / 100
    raw(
        """<path d="
        |M0,$y1l
        |L0,$y2l
        |L$marginL,$y2l
        |C$mr,$y2l $ml,$y2r $marginR,$y2r
        |L$width,$y2r
        |L$width,$y1r
        |L$marginR,$y1r
        |C$ml,$y1r $mr,$y1l $marginL,$y1l
        |L0,$y1l Z" fill="$color"/>""".trimMargin()
    )
}

private fun Unsafe.drawRequiredDice(
    minRequiredOutcome: DramaRollOutcome<UniformFairDieOutcome>?,
    width: Int,
    fontSize: Int,
    ty: Int
) {
    var x = width - fontSize / 2 - 1

    val secondOutcome = minRequiredOutcome?.secondOutcome
    if(secondOutcome != null && secondOutcome.face > 1) {
        drawDie(x, fontSize, ty, secondOutcome)
        x -= (2 * fontSize - 1)
    }

    val firstOutcome = minRequiredOutcome?.firstOutcome
    if(firstOutcome != null) {
        drawDie(x, fontSize, ty, firstOutcome)
    }
}

private fun Unsafe.drawDie(
    x: Int,
    fontSize: Int,
    ty: Int,
    firstOutcome: UniformFairDieOutcome
) {
    raw(
        """<circle cx=${x - fontSize / 2 - 1 } cy=${ty - fontSize / 2 + 2} r="${fontSize*8/10}" fill="white"/>""".trimMargin()
    )
    val t = firstOutcome.shortDescription
    val tx = if(t.length == 2) x else x - fontSize / 2 + 2
    raw("""<text x="$tx" y="$ty" text-anchor="end" font-size="$fontSize" stroke="black">$t</text>""")
}