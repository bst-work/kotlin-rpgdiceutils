import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "de.bstachmann"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.3.20"
}

allprojects {


    repositories {
        jcenter()
        maven {
            setUrl("https://kotlin.bintray.com/ktor")
        }
    }

    apply(plugin = "org.jetbrains.kotlin.jvm")


    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    dependencies {
        api(kotlin("stdlib-jdk8"))
        api("org.apache.commons:commons-math3:3.6.1")

        testImplementation("io.kotlintest:kotlintest-runner-junit5:3.1.10")
        testImplementation(kotlin("reflect"))

    }

}


task("stage") {
    dependsOn("build")

    doLast {
        println("okidoki")
    }
}
